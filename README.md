SimpleReddcoinWidget
===================

Clean looking, simple Reddcoin widget for Android.
Released on [Google Play](https://play.google.com/store/apps/details?id=com.iisurge.reddcoinwidget)
